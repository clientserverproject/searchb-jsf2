package it.unibas.searchbrani;

public class Costanti {

    public static final String REDIRECT = "?faces-redirect=true&includeViewParams=true";
    
    public static final String PERCORSO_CONF = "./progetto/conf/conf.properties";
    public static final String TAG_DAO_BRANI = "DAO_BRANI";
    
}
