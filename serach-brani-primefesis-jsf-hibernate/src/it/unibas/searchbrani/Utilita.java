package it.unibas.searchbrani;

import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Utilita {

    private static Logger logger = LoggerFactory.getLogger(Utilita.class);

    public static void setFlashProperty(String nome, Object valore) {
        Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();
        flash.put(nome, valore);
        flash.setKeepMessages(true);
    }

    public static Object getFlashProperty(String nome) {
        Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();
        return flash.get(nome);
    }
}
