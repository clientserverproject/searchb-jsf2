/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.searchbrani.modello;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class Archivio {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(Archivio.class);
    
    private List<Brano> brani = new ArrayList<>();
    private ParsingChain chain = new ParsingChain();

    public Archivio() {
    }

    public void setBrani(List<Brano> brani) {
        this.brani = brani;
    }
    
    
    public List<Brano> getBrani() {
        return brani;
    }

    public ParsingChain getChain() {
        return chain;
    }

    public void setChain(ParsingChain chain) {
        this.chain = chain;
    }
    
    public void addBrano(Brano brano){
        brani.add(brano);
    }
    
    public List<Brano> searchSongr(String key){
        if(key == null){
            LOGGER.error("Input nullo");
            throw new IllegalArgumentException("input nullo");
        }
        StringTokenizer tokenizer = new StringTokenizer(key, " ");
        List<Brano> braniRicercati = new ArrayList<>();
        List<Brano> appoggioRisultato = this.brani;
        while(tokenizer.hasMoreTokens()){
            String token = tokenizer.nextToken();
            LOGGER.debug("Key estratta: " + token);
            braniRicercati = chain.doChain(token, appoggioRisultato);
            appoggioRisultato = braniRicercati;
        }
        return braniRicercati;
    }
    
    
}
