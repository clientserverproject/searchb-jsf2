/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.searchbrani.modello;

import it.unibas.searchbrani.modello.headler.IParserHeadler;
import it.unibas.searchbrani.modello.headler.IgnoreEndCharacterHeadler;
import it.unibas.searchbrani.modello.headler.ManyTimesHeadler;
import it.unibas.searchbrani.modello.headler.NeverPresentHeadler;
import it.unibas.searchbrani.modello.headler.OnlyOneHeadler;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ParsingChain {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ParsingChain.class);
    
    private List<IParserHeadler> headlers = new ArrayList<>();

    public ParsingChain() {
        initHeadler();
    }
    
    
    public void addHeadler(IParserHeadler headler){
        if(headlers.contains(headler)){
            LOGGER.debug("headler gia' contenuto");
            return;
        }
        headlers.add(headler);
    }
    
    private void initHeadler(){
        IParserHeadler parser = new OnlyOneHeadler();
        headlers.add(parser);
        parser = new NeverPresentHeadler();
        headlers.add(parser);
        parser = new ManyTimesHeadler();
        headlers.add(parser);
        parser = new IgnoreEndCharacterHeadler();
        headlers.add(parser);
    }
    
    public List<Brano> doChain(String message, List<Brano> repoBrani){
        List<Brano> brani = new ArrayList<>();
        List<Brano> appoggioBraniRepo = repoBrani;
        Iterator iterator = headlers.iterator();
        while(iterator.hasNext()){
            IParserHeadler parser = (IParserHeadler) iterator.next();
            LOGGER.debug("Dimenzione lista prima dell'esecuzione del metodo doRun: " + brani.size());
            brani = parser.doRun(message, appoggioBraniRepo);
            appoggioBraniRepo = brani;
            LOGGER.debug("Dimenzione lista dopo dell'esecuzione del metodo doRun: " + brani.size());
        }
        return brani;
    }
}
