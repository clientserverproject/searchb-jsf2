/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.searchbrani.modello.headler;

import it.unibas.searchbrani.modello.Brano;
import java.util.List;

/**
 *
 * @author Vincenzo Palazzo
 */
public interface IParserHeadler {
    
    List<Brano> doRun(String message, List<Brano> repoBrano);
}
