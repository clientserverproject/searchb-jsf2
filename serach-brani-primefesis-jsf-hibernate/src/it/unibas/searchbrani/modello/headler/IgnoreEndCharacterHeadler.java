/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.searchbrani.modello.headler;

import it.unibas.searchbrani.modello.Brano;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class IgnoreEndCharacterHeadler implements IParserHeadler{

    private static final Logger LOGGER = LoggerFactory.getLogger(IgnoreEndCharacterHeadler.class);
    
    private String simple = "<";
    
    @Override
    public List<Brano> doRun(String message, List<Brano> repoBrano) {
       if(message == null || repoBrano == null){
            LOGGER.error("Paramentri in ingresso nulli");
            throw new IllegalArgumentException("Parametri in ingresso nulli");
        }
        List<Brano> brani = new ArrayList<>();
        
        String simboloRicerca = message.substring(0, 1);
        String parolaRicercata = message.substring(1, message.length() - 1);
        
        LOGGER.debug("Simbolo estratto: " + simboloRicerca);
        LOGGER.debug("parola  estratto: " + parolaRicercata);
        
        if(!simboloRicerca.trim().equalsIgnoreCase(simple)){
            return repoBrano;
        }
        
        for(Brano brano : repoBrano){
            String testo = brano.getTesto().toLowerCase();
            
            if(testo.contains(parolaRicercata.toLowerCase())){
                brani.add(brano);
            }
        }
        return brani;
    }
    
}
