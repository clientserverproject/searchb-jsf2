/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.searchbrani.modello.headler;

import it.unibas.searchbrani.modello.Brano;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ManyTimesHeadler implements IParserHeadler{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ManyTimesHeadler.class);
    
    private String simbolo = "*";

    @Override
    public List<Brano> doRun(String message, List<Brano> repoBrano) {
        if(message == null || repoBrano == null){
            LOGGER.error("Paramentri in ingresso nulli");
            throw new IllegalArgumentException("Parametri in ingresso nulli");
        }
        List<Brano> brani = new ArrayList<>();
        
        String simboloRicerca = message.substring(0, 1);
        String parolaRicercata = message.substring(1, message.length());
        
        if(!simboloRicerca.trim().equalsIgnoreCase(simbolo)){
            return repoBrano;
        }
        
        for(Brano brano : repoBrano){
            int value = 0;
            StringTokenizer tokenizer = new StringTokenizer(brano.getTesto(), " ");
            while(tokenizer.hasMoreTokens()){
                String token = tokenizer.nextToken();
                if(token.toLowerCase().contains(parolaRicercata.toLowerCase())){
                    value++;
                }
            }
            if(value > 1){
                brani.add(brano);
            }
        }
        return brani;
    }
    
}
