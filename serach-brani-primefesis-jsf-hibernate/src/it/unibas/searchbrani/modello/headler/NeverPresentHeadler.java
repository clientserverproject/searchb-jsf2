/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.searchbrani.modello.headler;

import it.unibas.searchbrani.modello.Brano;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class NeverPresentHeadler implements IParserHeadler {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(NeverPresentHeadler.class);
    
    private String simbolo = "-";
    
    @Override
    public List<Brano> doRun(String message, List<Brano> repoBrano) {
        if (message == null || repoBrano == null) {
            LOGGER.error("Paramentri in ingresso nulli");
            throw new IllegalArgumentException("Parametri in ingresso nulli");
        }
        List<Brano> brani = new ArrayList<>();
        
        String simboloRicerca = message.substring(0, 1);
        String parolaRicercata = message.substring(1, message.length());
        
        if (!simboloRicerca.trim().equalsIgnoreCase(simbolo)) {
            return repoBrano;
        }
        
        LOGGER.debug("Simbolo estratto: " + simboloRicerca);
        LOGGER.debug("Parola estratta: " + parolaRicercata);
        
        for (Brano brano : repoBrano) {
            String testo = brano.getTesto().toLowerCase();
            if (!testo.contains(parolaRicercata.toLowerCase())) {
                LOGGER.debug("Il brano: " + brano.getTitolo() + " non contiene la parola: " + parolaRicercata);
                brani.add(brano);
            }
        }
        
        return brani;
    }
    
}
