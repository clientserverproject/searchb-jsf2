/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.searchbrani.modello.headler;

import it.unibas.searchbrani.modello.Brano;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class OnlyOneHeadler implements IParserHeadler{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(OnlyOneHeadler.class);
    
    private String simbolo = "+";

    @Override
    public List<Brano> doRun(String message, List<Brano> repoBrano) {
        if(message == null || repoBrano == null){
            LOGGER.error("Paramentri in ingresso nulli");
            throw new IllegalArgumentException("Parametri in ingresso nulli");
        }
        List<Brano> braniCompatibili = new ArrayList<>();
        
        String simboloRicerca = message.substring(0, 1);
        String parolaRicerca = message.substring(1, message.length());
        
        LOGGER.debug("Metodo di ricerca della stringa nel brano: " + simboloRicerca);
        LOGGER.debug("Stringa nel brano: " + parolaRicerca);
        
        if(!simboloRicerca.trim().equalsIgnoreCase(simbolo.trim())){
            LOGGER.debug("Headler" + simbolo +" non compatibile con " + simboloRicerca );
            return repoBrano;
        }
        
        for(Brano brano : repoBrano){
            LOGGER.debug("Brano esaminato: " + brano.getTitolo());
            brano.getTesto().replace(",", " , ");
            brano.getTesto().replace(";", " ; ");
            brano.getTesto().replace(".", " . ");
            //brano.getTesto().replaceAll("?", " ? ");
            //brano.getTesto().replaceAll("!", " ! ");
            StringTokenizer tokenizer = new StringTokenizer(brano.getTesto(), " ");
            int voltrePresenzaParola = 0;
            while (tokenizer.hasMoreElements()) {        
                String token = tokenizer.nextToken();
                LOGGER.debug("String esamianata: " + token);
                if(token.toLowerCase().contains(parolaRicerca.toLowerCase())){
                    LOGGER.debug("Ho trovato un parola compatibile nel brano: " + brano.getTitolo());
                    voltrePresenzaParola++;
                    LOGGER.debug("Volte che si e' presentato: " + voltrePresenzaParola);
                }
            }
            if(voltrePresenzaParola == 1){
                LOGGER.debug("Brano compatibole: " + brano.getTitolo());
                braniCompatibili.add(brano);
            }
        }
        
        return braniCompatibili;
    }

    
    
}
