package it.unibas.searchbrani.persistenza;

import it.unibas.searchbrani.eccezioni.DAOException;
import it.unibas.searchbrani.modello.Utente;

public interface IDAOUtente extends IDAOGenerico<Utente> {
     
    public Utente findByNomeUtente(String nomeUtente) throws DAOException;

    public void save(Utente utente) throws DAOException;

    public void delete(Utente utente) throws DAOException;

}
