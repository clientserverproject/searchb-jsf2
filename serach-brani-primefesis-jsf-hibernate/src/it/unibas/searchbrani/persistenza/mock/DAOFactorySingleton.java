/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.searchbrani.persistenza.mock;

import it.unibas.searchbrani.Costanti;
import java.net.URL;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class DAOFactorySingleton {
    
    private static final Logger LOGGER = LoggerFactory.getLogger("DAOFactorySingleton");

    private static DAOFactorySingleton SINGLETON;

    private IDAOBrani daoBrani;
    
    public static DAOFactorySingleton getIstance() {
        if (SINGLETON == null) {
            SINGLETON = new DAOFactorySingleton();
        }
        return SINGLETON;
    }

    private DAOFactorySingleton() {
        load();
    }

    public IDAOBrani getDaoBrani() {
        return daoBrani;
    }

    private void load() {
        Properties properties = new Properties();
        try {
            LOGGER.error(ClassLoader.getSystemResourceAsStream(Costanti.PERCORSO_CONF) + " ");
            LOGGER.error(getClass().getResourceAsStream(Costanti.PERCORSO_CONF) + "");
            //properties.load(getClass().getResourceAsStream(Costanti.PERCORSO_CONF));
            //String packegeClass = properties.getProperty(Costanti.TAG_DAO_BRANI);
            //daoBrani = (IDAOBrani) getClass().getClassLoader().loadClass(packegeClass).newInstance();
            daoBrani = new DAOMock();
        } catch (Exception ex) {
            LOGGER.error("Errore del tipo: " + ex.getLocalizedMessage());
            ex.printStackTrace();
        }
    }

}
