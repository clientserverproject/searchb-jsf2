/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.searchbrani.persistenza.mock;

import it.unibas.searchbrani.modello.Brano;
import it.unibas.searchbrani.persistenza.mock.IDAOBrani;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class DAOMock implements IDAOBrani{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(DAOMock.class);

    @Override
    public List<Brano> load() {
        List<Brano> brani = new ArrayList<>();
        
        String testo = "Quel giorno di Giugno,era un mercoledì,inizia la mia storia,immerso in mille pensieri all’improvviso mi	accorsi \n" +
                    "che lei era proprio lì,davanti a me, con un torta di mele e noci dall’aspetto invitante. Mi sentivo già a casa.";
        
        Brano brano = new Brano("La mia storia", testo, new GregorianCalendar());
        brani.add(brano);
        
        testo = "A dire	il vero	cercavo	una casa per le	vacanze	che avesse un albero secolare, enorme, sotto cui sostare nei caldi \n" +
                "pomeriggi d’estate ascoltando il frinire ritmico delle cicale ed immaginando la mia vecchia casa vacanze.";
        
        brano = new Brano("Vacanza in puglia", testo, new GregorianCalendar());
        brani.add(brano);
        
        testo = "Ed è così che finii ad	Alberobello, immerso tra i pittoreschi trulli che delineano il costone ovest del paesaggio \n" +
                "pugliese, dove trovi un solo albergo, bianco anch’esso, anch’esso un trullo, ma accogliente come casa.";
        
        brano = new Brano("Alberobello", testo, new GregorianCalendar());
        brani.add(brano);
        
        LOGGER.debug("Brani caricati dal mock: " + brani.size());
        
        return brani;
    }
    
}
