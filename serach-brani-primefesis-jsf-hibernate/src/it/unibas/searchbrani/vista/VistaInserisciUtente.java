/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.searchbrani.vista;

import it.unibas.searchbrani.modello.Utente;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.FlowEvent;

/**
 *
 * @author Vincenzo Palazzo
 */
@ManagedBean
@ViewScoped
//Forse andrebbe meglio chiamarla vistaOperazioniUtente ma non cambio il nome
//Solo perchè il refactoring di netbinse non funziona sulle pagine jsf
public class VistaInserisciUtente implements Serializable{
      
    private Utente utente = new Utente();
    private List<Utente> utenti = new ArrayList<>();

    private boolean skip;

    public List<Utente> getUtenti() {
        return utenti;
    }

    public void setUtenti(List<Utente> utenti) {
        this.utenti = utenti;
    }

    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }
     
    public boolean isSkip() {
        return skip;
    }
 
    public void setSkip(boolean skip) {
        this.skip = skip;
    }
     
    public String onFlowProcess(FlowEvent event) {
        if(skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        }
        else {
            return event.getNewStep();
        }
    }

    
    
}
