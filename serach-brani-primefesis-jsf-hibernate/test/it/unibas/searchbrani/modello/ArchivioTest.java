/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.searchbrani.modello;

import it.unibas.searchbrani.persistenza.mock.DAOFactorySingleton;
import it.unibas.searchbrani.persistenza.mock.IDAOBrani;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ArchivioTest {
    
    IDAOBrani dao;
    Archivio archivio;
    
    @Before
    public void initTest(){
        dao = DAOFactorySingleton.getIstance().getDaoBrani();
        archivio = new Archivio();
        archivio.setBrani(dao.load());
    }
    
    @Test
    public void testUnaChiave(){
        List<Brano> brani = archivio.searchSongr("*casa");
        TestCase.assertEquals(1, brani.size());
    }
    
    @Test
    public void testCombinazioneUno(){
        List<Brano> brani = archivio.searchSongr("+casa –scuola");
        TestCase.assertEquals(2, brani.size());
        TestCase.assertEquals("La mia storia", brani.get(0).getTitolo());
        TestCase.assertEquals("Alberobello", brani.get(1).getTitolo());
    }
    
    @Test
    public void testCombinazioneDue(){
        List<Brano> brani = archivio.searchSongr("*casa –scuola");
        TestCase.assertEquals(1, brani.size());
        TestCase.assertEquals("Vacanza in puglia", brani.get(0).getTitolo());
    }
    
    @Test
    public void testTripaUno(){
        List<Brano> brani = archivio.searchSongr("+casa –scuola +albergo");
        TestCase.assertEquals(1, brani.size());
        TestCase.assertEquals("Alberobello", brani.get(0).getTitolo());
    }
    
    @Test
    public void testTripaDue(){
        List<Brano> brani = archivio.searchSongr("<casa <scuola -albergo");
        TestCase.assertEquals(0, brani.size());
    }
    
    @Test
    public void testTripaTre(){
        List<Brano> brani = archivio.searchSongr("<pomeriggio *casa");
        TestCase.assertEquals(1, brani.size());
        TestCase.assertEquals("Vacanza in puglia", brani.get(0).getTitolo());
    }
    
    @Test
    public void testCoppiaSecondoTestoUno(){
        initNewSongr();
        List<Brano> brani = archivio.searchSongr("+cercavo +ok");
        TestCase.assertEquals(0, brani.size());
    }
    @Test
    public void testCoppiaSecondoTestoTre(){
        initNewSongr();
        List<Brano> brani = archivio.searchSongr(" +ok");
        TestCase.assertEquals(1, brani.size());
        TestCase.assertEquals("Kill Them all", brani.get(0).getTitolo());
    }
    @Test
    public void testCoppiaSecondoTestoDue(){
        initNewSongr();
        List<Brano> brani = archivio.searchSongr("+cercavo");
        TestCase.assertEquals(1, brani.size());
        TestCase.assertEquals("Super eroe", brani.get(0).getTitolo());
    }
    
    @Test
    public void testCoppiaSecondoTestoQuattro(){
        initNewSongr();
        List<Brano> brani = archivio.searchSongr(" <o");
        TestCase.assertEquals(2, brani.size());
         TestCase.assertEquals("Super eroe", brani.get(0).getTitolo());
        TestCase.assertEquals("Kill Them all", brani.get(1).getTitolo());
    }
    @Test
    public void testCoppiaSecondoTestoCinque(){
        initNewSongr();
        List<Brano> brani = archivio.searchSongr("<nome");
        TestCase.assertEquals(1, brani.size());
        TestCase.assertEquals("Kill Them all", brani.get(0).getTitolo());
    }
    
    
    private void initNewSongr(){
        List<Brano> brani = new ArrayList<>();
        
        String testo = "Cercavo il tuo sorriso in quello delle altre persone\n" +
                        "Ma qua nessuno mi sorride come lo facevi tu\n" +
                        "Per ricordarti non mi serve una canzone\n" +
                        "Sono un supereroe, combattero' pr noi ma lo faro' quaggiu'";
        
        Brano brano = new Brano("Super eroe", testo, new GregorianCalendar());
        brani.add(brano);
        
        testo = "Fumo piante coltivate col seme della follia, yeah! La mia compagnia è fatta di vicodin, \n" +
        "moriamo d’asfissia nello spazio fra i miei ventricoli, sono il prossimo che imiti, \n" +
        "il nome che adesso digiti, se senti questo testo attento ai contenuti impliciti, Ok?";       
        brano = new Brano("Kill Them all", testo, new GregorianCalendar());
        brani.add(brano);
        archivio.setBrani(brani);
    }
    
}
