/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.searchbrani.modello;

import it.unibas.searchbrani.persistenza.mock.DAOFactorySingleton;
import it.unibas.searchbrani.persistenza.mock.IDAOBrani;
import java.util.List;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ParserChainTest {
    
    IDAOBrani dao;
    Archivio archivio;
    //TODO mancano altri testi su altri brani
    @Before
    public void initTestData(){
        dao = DAOFactorySingleton.getIstance().getDaoBrani();
        archivio = new Archivio();
        archivio.setBrani(dao.load());
    }
    
    @Test
    public void testHeadlerOnlyOne(){
        String key = "+casa";
        ParsingChain chain = archivio.getChain();
        List<Brano> brani = chain.doChain(key, archivio.getBrani());
        TestCase.assertEquals(2, brani.size());
        TestCase.assertEquals("La mia storia", brani.get(0).getTitolo());
        TestCase.assertEquals("Alberobello", brani.get(1).getTitolo());
    }
    
    @Test
    public void testHeadlerNeverPresentOne(){
        String key = "-casa";
        ParsingChain chain = archivio.getChain();
        List<Brano> brani = chain.doChain(key, archivio.getBrani());
        TestCase.assertEquals(0, brani.size());
    }
    
    @Test
    public void testHeadlerNeverPresentTwo(){
        String key = "-scuola";
        ParsingChain chain = archivio.getChain();
        List<Brano> brani = chain.doChain(key, archivio.getBrani());
        TestCase.assertEquals(3, brani.size());
        TestCase.assertEquals("La mia storia", brani.get(0).getTitolo());
        TestCase.assertEquals("Vacanza in puglia", brani.get(1).getTitolo());
        TestCase.assertEquals("Alberobello", brani.get(2).getTitolo());
    }
    
    @Test
    public void testHeadlerManyTimesOne(){
        String key = "*casa";
        ParsingChain chain = archivio.getChain();
        List<Brano> brani = chain.doChain(key, archivio.getBrani());
        TestCase.assertEquals(1, brani.size());
        TestCase.assertEquals("Vacanza in puglia", brani.get(0).getTitolo());
    }
    
    @Test
    public void testHeadlerIgnoreEndCharacterOne(){
        String key = "<casa";
        ParsingChain chain = archivio.getChain();
        List<Brano> brani = chain.doChain(key, archivio.getBrani());
        TestCase.assertEquals(3, brani.size());
        TestCase.assertEquals("La mia storia", brani.get(0).getTitolo());
        TestCase.assertEquals("Vacanza in puglia", brani.get(1).getTitolo());
        TestCase.assertEquals("Alberobello", brani.get(2).getTitolo());
    }
    
    @Test
    public void testHeadlerIgnoreEndCharacterTwo(){
        String key = "<pomeriggio";
        ParsingChain chain = archivio.getChain();
        List<Brano> brani = chain.doChain(key, archivio.getBrani());
        TestCase.assertEquals(1, brani.size());
        TestCase.assertEquals("Vacanza in puglia", brani.get(0).getTitolo());
    }
    
}
