/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.searchbrani.persistenza;

import it.unibas.searchbrani.modello.Brano;
import it.unibas.searchbrani.persistenza.mock.DAOFactorySingleton;
import it.unibas.searchbrani.persistenza.mock.IDAOBrani;
import java.util.List;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;


/**
 *
 * @author Vincenzo Palazzo
 */
public class DAOTest {
    
    IDAOBrani dao;
    
   @Before
   public void initTestsDate(){
       dao = DAOFactorySingleton.getIstance().getDaoBrani();
   }
   
   @Test
   public void testDaoNotNull(){
       TestCase.assertNotNull(dao);
   }
   
   @Test
   public void testDaoDimensionData(){
       List<Brano> brani = dao.load();
       TestCase.assertEquals(3, brani.size());
   }
       
    
}
