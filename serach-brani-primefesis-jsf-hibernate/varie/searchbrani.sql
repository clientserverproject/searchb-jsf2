
    create table brani (
        id int8 not null,
        data timestamp,
        testo varchar(255) not null,
        titolo varchar(255) not null,
        primary key (id)
    );

    create table ricerche (
        id int8 not null,
        calendar timestamp not null,
        chiaveDiRicerca varchar(50) not null,
        numeroRisultati int4 not null,
        primary key (id)
    );

    create table utente (
        id int8 not null,
        attivo boolean not null,
        lastLogin timestamp,
        nome varchar(255),
        nomeUtente varchar(255),
        password varchar(255),
        ruolo varchar(255),
        primary key (id)
    );

    alter table brani 
        add constraint UK_buh1ilcxtm1wsk1dhaxyca55e unique (testo);

    alter table utente 
        add constraint UK_7hipuu05v6vcqr7wbl8q7p4t2 unique (nomeUtente);

    create table hibernate_sequences (
         sequence_name varchar(255),
         sequence_next_hi_value int4 
    );
